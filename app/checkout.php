<?php
require_once("vendor/autoload.php");
require_once("includes/braintree_init.php");
require_once("includes/googledrive_init.php");


$amount = $_POST["amount"];
if ($amount == "other") {
  $amount = $_POST["amountOther"];
}
$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$email = $_POST["email"];
$subscribeTrue = $_POST["subscribe"];
$donordisplayTrue = $_POST["donor_display"];
$referral = $_POST["referral"];
$phone = $_POST["phone"];
$company = $_POST["company"];
$streetAddress = $_POST["streetAddress"];
$suburb = $_POST["locality"];
$region = $_POST["region"];
$postalCode = $_POST["postalCode"];
$countryCode = $_POST["countryCodeAlpha2"];

// Bridging the gap API key
$mailchimp_api_key = '4715c214e3d31924ff701004d9965fd3-us17';
$mailchimp_list_id = 'd1cfa20b20';
$mailchimp_subscribe_email = $email;
$mailchimp_subscribe_status = 'subscribed';
$mailchimp_signup_fields = array(
  'FNAME' => $firstName,
  'LNAME' => $lastName,
  'REFERRAL' => $referral,
  'PHONE' => $phone,
  'COMPANY' => $company,
  'STREETADDR' => $streetAddress,
  'LOCALITY' => $suburb,
  'REGION' => $region,
  'POSTCODE' => $postalCode,
  'COUNTRY' => $countryCode
);

// Get unique transaction hash for Braintree verification
$nonce = $_POST["payment_method_nonce"];

//-----------------------------------------------------------------------------------//
// Braintree Process Sale
//-----------------------------------------------------------------------------------//
$result = Braintree\Transaction::sale([
    'amount' => $amount,
    'paymentMethodNonce' => $nonce,
    'customer' => [
      'firstName' => $firstName,
      'lastName' => $lastName,
      'phone' => $phone,
      'company' => $company,
      'email' => $email
    ],
    'billing' => [
      'streetAddress' => $streetAddress,
      'locality' => $suburb,
      'region' => $region,
      'postalCode' => $postalCode,
      'countryCodeAlpha2' => $countryCode
    ],
    'customFields' => [
      'subscribe' => $subscribeTrue,
      'referral' => $referral
    ],
    'options' => [
        'submitForSettlement' => true
    ]
]);
//echo "<script>console.log(".json_encode(var_export($result, true)).");</script>";
if ($result->success || !is_null($result->transaction)) {
    $transaction = $result->transaction;

    // Check if news/mailchimp opt-in
    if ($subscribeTrue) {
	    // Send field values to Mailchimp Bridge the Gap List
	    if ( function_exists('kwp_mailchimp_update_subscriber_status')) {
	    	kwp_mailchimp_update_subscriber_status(
	    		$mailchimp_list_id,
	    		$mailchimp_api_key,
	    		$mailchimp_subscribe_email,
	    		$mailchimp_subscribe_status,
	    		$mailchimp_signup_fields
	    	);
	    }
    }

    // Check if donor list opt-in
    if ($donordisplayTrue) {
	    //echo "<script>console.log('Oopt in accepted for donordisplayTrue');</script>";
	    if ( function_exists('kwp_get_google_donors_list')) {

			// Get all rows in the sheet
	    	$listFeed_rows = kwp_get_google_donors_list();
			//echo '<pre>$listFeed_rows: ' . var_export($listFeed_rows, true) . '</pre>';

			// @var ListEntry
			//$donors = array();
			//foreach ($listFeed_rows->getEntries() as $entry) { $donors[] = $entry->getValues(); }

			//$donors_output = '';
			//foreach ($donors as $donor) {
			   //echo '<pre>$donor: ' . var_export($donor, true) . '</pre>';
			   //$donors_output .= $donor['firstname'].' '.$donor['lastname'].', '.$donor['amount'].'<br/>';
			//}
			//echo '<p>'.$donors_output.'</p>';

			//Add a new row into the Google Sheet of Donors
			$listFeed_rows->insert([ 'firstname' => $firstName, 'lastname' => $lastName, 'amount' => $amount ]);

	    }
    }



    header("Location: transaction.php?id=" . $transaction->id);
} else {

    $errorString = "";

    foreach($result->errors->deepAll() as $error) {
        $errorString .= 'Error: ' . $error->code . ": " . $error->message . "\n";
    }

    $_SESSION["errors"] = $errorString;
    header("Location: donate.php");
}


//-----------------------------------------------------------------------------------//
// Mailchimp API 3.0 – subscribe
//-----------------------------------------------------------------------------------//
function kwp_mailchimp_update_subscriber_status( $list_id, $api_key, $email, $status, $merge_fields = array(
  'FNAME' => '',
  'LNAME' => '',
  'REFERRAL' => '',
  'PHONE' => '',
  'COMPANY' => '',
  'STREETADDR' => '',
  'LOCALITY' => '',
  'REGION' => '',
  'POSTCODE' => '',
  'COUNTRY' => '')
  ){
	$data = array(
		'apikey'        => $api_key,
    'email_address' => $email,
		'status'        => $status,
		'merge_fields'  => $merge_fields
	);
	$mch_api = curl_init(); // initialize cURL connection
	curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
	curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
	curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
	curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
	curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
	curl_setopt($mch_api, CURLOPT_POST, true);
	curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json

	$result = curl_exec($mch_api);
	return $result;
}



 ?>
