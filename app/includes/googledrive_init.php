<?php

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

function kwp_get_google_donors_list() {

	putenv('GOOGLE_APPLICATION_CREDENTIALS=' . __DIR__ . '/client_secret.json');
	$client = new Google_Client;
	$client->useApplicationDefaultCredentials();

	$client->setApplicationName("Something to do with my representatives");
	$client->setScopes(['https://www.googleapis.com/auth/drive','https://spreadsheets.google.com/feeds']);

	if ($client->isAccessTokenExpired()) {
		$client->refreshTokenWithAssertion();
	}

	$accessToken = $client->fetchAccessTokenWithAssertion()["access_token"];
	ServiceRequestFactory::setInstance(
		new DefaultServiceRequest($accessToken)
	);

	// Get our spreadsheet
	$spreadsheet = (new Google\Spreadsheet\SpreadsheetService)
	   ->getSpreadsheetFeed()
	   ->getByTitle('BTG Donors');
	//print_r($spreadsheet);
	//echo '<pre>$spreadsheet: ' . var_export($spreadsheet, true) . '</pre>';

	// Get the first worksheet (tab)
	$worksheets = $spreadsheet->getWorksheetFeed()->getEntries();
	$worksheet = $worksheets[0];
	//echo '<pre>$worksheets: ' . var_export($worksheets, true) . '</pre>';

	$listFeed_rows = $worksheet->getListFeed();
	//echo '<pre>$listFeed_rows: ' . var_export($listFeed_rows, true) . '</pre>';

	return $listFeed_rows;

}