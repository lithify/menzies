<?php
session_start();
require_once("vendor/autoload.php");

if (file_exists(__DIR__ . "/.env")) {
    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();
}


Braintree\Configuration::environment(getenv('BT_ENVIRONMENT'));
Braintree\Configuration::merchantId(getenv('BT_MERCHANT_ID'));
Braintree\Configuration::publicKey(getenv('BT_PUBLIC_KEY'));
Braintree\Configuration::privateKey(getenv('BT_PRIVATE_KEY')); ?>

<!doctype html>
<html lang="">
<head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bridging the Gap Foundation - Donate</title>

  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- build:css styles/vendor.css -->
  <!-- bower:css -->
  <!-- endbower -->
  <!-- endbuild -->

  <!-- build:css styles/main.css -->
  <link rel="stylesheet" href="styles/main.css">
  <!-- endbuild -->

  <script src="https://js.braintreegateway.com/web/dropin/1.9.3/js/dropin.min.js"></script>



</head>

<body>

  <div id="app">
    <div class="nav-top-bar">
      <div class="play-arrow"><a href="" id="play-vid"><span class="play-vid-inner"><span class="d-none d-md-block">Play Video</span></span></a></div>
      <div class="donate-btn-top">
        <a href="donate.php" class="btn">Donate</a>
      </div>
    </div>

<div class="donate-bar nav-up">
      <div class="container">
        <div class="main-nav">
          <a href="about-us.html" class="main-nav-item">Our Mission</a>
          <a href="who.html" class="main-nav-item">What we do</a>
          <a href="how.html" class="main-nav-item">Key Projects</a>
          <a href="latest-news.html" class="main-nav-item">Latest News</a>
          <a href="who-we-are.html" class="main-nav-item">Who we are</a>
          <a href="contact-us.html" class="main-nav-item">Contact</a>
          <a href="volunteer-fundraising.html" class="main-nav-item">Fundraising</a>
        </div>
      </div>


      <button class="d-xl-none hamburger hamburger--squeeze" type="button" id="hamburger">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
    </div>

    <section class="fullwidth fixed sub" style="background-image: url(images/fullwidth-toddler3.jpg);">
      <div class="banner-wrap">
        <nav class="topnav">

          <div class="logo-group">
            <a href="/" class="top-row-link"><img src="images/bridging-the-gap-logo.svg" alt="Bridging The Gap" class="main-logo img-fluid"></a>
            <div class="bott-row">
              <div class="left-col">
                <img src="images/menzies-logo-white.svg" alt="Menzies Logo" class="menzies-logo img-fluid">
              </div>
              <div class="right-col">
                <img src="images/charles-darwin-logo-white.svg" alt="Charles Darwin University Logo" class="charles-logo img-fluid">
              </div>
            </div>
          </div>
        </nav>
      </div>
    </section>

    <main class="content">


        <section class="donate yellow-bg">
          <div class="container">
            <form method="post" id="payment-form" action="checkout.php" class="needs-validation" novalidate>
            <div class="row">
              <div class="col-md-5">
                <h2>Donate Now</h2>
                <a href="https://www.acnc.gov.au/RN52B75Q?ID=AC5800C9-1C48-4751-B1BF-BF26F80D6083&noleft=1">
                  <img class="acnc-logo" style="max-width: 150px; margin-bottom: 100px;" src="images/ACNC-Registered-Charity-Logo_RGB.png">
                </a>
              </div>
            </div>

            <div class="row">
              <div class="col-md-8">
                <a href="" id="donateOnce" class="donate-toggle selected">Donate once</a>
                <!-- <a href="" id="donateMonthly" class="donate-toggle">Donate monthly</a> -->
              </div>
            </div>

            <div class="row">
              <div class="col-md-9">
                <div class="donate-amt">
                  <input type="radio" class="form-control" id="amt50" name="amount" value="50" required><label class="round-btn" for="amt50"> $50</label>
                  <input type="radio" class="form-control" id="amt100" name="amount" value="100" required><label class="round-btn" for="amt100"> $100</label>
                  <input type="radio" class="form-control" id="amt250" name="amount" value="250" required><label class="round-btn" for="amt250"> $250</label>
                  <input type="radio" class="form-control" id="amt1000" name="amount" value="1000" required><label class="round-btn" for="amt1000"> $1000</label>
                  <input type="radio" class="form-control" id="other" name="amount" value="other" required><label class="round-btn" for="other">OTHER</label>
                  <div class="invalid-feedback">Please select donation amount</div>
                </div>
              </div>

              </div>
              <div class="row">
              <div class="col-md-6">
  	            <div class="amtOtherBlock" id="amtOtherBlock"><span class="prefix">$ </span><input type="number" id="amtOther" name="amountOther" placeholder="0.00" class="form-control"></div>

                <div class="form-row">
  	              <div class="col-sm-6">
                    <label for="firstName">First name</label>
                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First name" required>
                    <div class="invalid-feedback">
                      Please enter first name
                    </div>
  	              </div>

                  <div class="col-sm-6">
                    <label for="lastName">Last name</label>
                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last name" required>
                    <div class="invalid-feedback">
                      Please enter last name
                    </div>
                  </div>
                </div>

                <!-- <div class="form-row">
  	              <div class="col-sm-6">
                  <label for="dob">Date of birth</label>
                  <input type="date" class="form-control" id="dob" name="dob" placeholder="DD/MM/YYYY">
                  </div>
                </div> -->

                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="example@example.com" required>
                  <div class="invalid-feedback">
                    Please enter email address
                  </div>
                </div>

                <div class="form-group">
                  <div class="form-check">
                    <label for="subscribe" class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="yes" id="subscribe" name="subscribe"> Keep up to date with Bridging the Gap Foundation news.</label>
                    <label for="donor_display" class="form-check-label">
                    <input class="form-check-input" type="checkbox" value="yes" id="donor_display" name="donor_display"> Display my name on the Bridging The Gap donors list.</label>
                  </div>
                </div>

                <div class="form-group">
                  <select name="referral" id="referral" class="custom-select">
  	                <option selected> How did you hear about us?</option>
                    <option value="Google">Google</option>
                    <option value="Google">Social Media</option>
                    <option value="Google">Friends or Family</option>
                    <option value="Other">Other</option>
                  </select>
                </div>

                <div class="bt-drop-in-wrapper">
                    <div id="bt-dropin"></div>
                </div>

                <div class="form-row">
                  <div class="col-sm-6">
                  <label for="phone">Phone</label>
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                </div>
                  <div class="col-sm-6">
                  <label for="company">Company name (if applicable)</label>
                  <input type="text" class="form-control" id="company" name="company" placeholder="Company">
                </div>
                  </div>

                  <div class="form-row">
                    <div class="col-sm-7">
                  <label for="streetAddress">Address</label>
                  <input type="text" class="form-control" id="streetAddress" name="streetAddress" placeholder="Address" required>
                  </div>
                  <div class="col-sm-5">
                    <label for="locality">Suburb</label>
                    <input type="text" class="form-control" id="locality" name="locality" placeholder="Suburb" required>
                    </div>
                </div>

                <div class="form-row">
                  <div class="col-7 col-sm-3">
                      <label for="region">State</label>
                      <input type="text" class="form-control" id="region" name="region" required>
                  </div>
                  <div class="col-5 col-sm-4">
                    <label for="postalCode">Postcode</label>
                    <input type="text" class="form-control" id="postalCode" name="postalCode" required>
                  </div>
                  <div class="col-sm-5">
                     <label for="countryCodeAlpha2">Country</label>
                     <select name="countryCodeAlpha2" id="countryCodeAlpha2" class="custom-select">
                    	<option value="AF">Afghanistan</option>
                    	<option value="AX">Åland Islands</option>
                    	<option value="AL">Albania</option>
                    	<option value="DZ">Algeria</option>
                    	<option value="AS">American Samoa</option>
                    	<option value="AD">Andorra</option>
                    	<option value="AO">Angola</option>
                    	<option value="AI">Anguilla</option>
                    	<option value="AQ">Antarctica</option>
                    	<option value="AG">Antigua and Barbuda</option>
                    	<option value="AR">Argentina</option>
                    	<option value="AM">Armenia</option>
                    	<option value="AW">Aruba</option>
                    	<option value="AU" selected>Australia</option>
                    	<option value="AT">Austria</option>
                    	<option value="AZ">Azerbaijan</option>
                    	<option value="BS">Bahamas</option>
                    	<option value="BH">Bahrain</option>
                    	<option value="BD">Bangladesh</option>
                    	<option value="BB">Barbados</option>
                    	<option value="BY">Belarus</option>
                    	<option value="BE">Belgium</option>
                    	<option value="BZ">Belize</option>
                    	<option value="BJ">Benin</option>
                    	<option value="BM">Bermuda</option>
                    	<option value="BT">Bhutan</option>
                    	<option value="BO">Bolivia, Plurinational State of</option>
                    	<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                    	<option value="BA">Bosnia and Herzegovina</option>
                    	<option value="BW">Botswana</option>
                    	<option value="BV">Bouvet Island</option>
                    	<option value="BR">Brazil</option>
                    	<option value="IO">British Indian Ocean Territory</option>
                    	<option value="BN">Brunei Darussalam</option>
                    	<option value="BG">Bulgaria</option>
                    	<option value="BF">Burkina Faso</option>
                    	<option value="BI">Burundi</option>
                    	<option value="KH">Cambodia</option>
                    	<option value="CM">Cameroon</option>
                    	<option value="CA">Canada</option>
                    	<option value="CV">Cape Verde</option>
                    	<option value="KY">Cayman Islands</option>
                    	<option value="CF">Central African Republic</option>
                    	<option value="TD">Chad</option>
                    	<option value="CL">Chile</option>
                    	<option value="CN">China</option>
                    	<option value="CX">Christmas Island</option>
                    	<option value="CC">Cocos (Keeling) Islands</option>
                    	<option value="CO">Colombia</option>
                    	<option value="KM">Comoros</option>
                    	<option value="CG">Congo</option>
                    	<option value="CD">Congo, the Democratic Republic of the</option>
                    	<option value="CK">Cook Islands</option>
                    	<option value="CR">Costa Rica</option>
                    	<option value="CI">Côte d'Ivoire</option>
                    	<option value="HR">Croatia</option>
                    	<option value="CU">Cuba</option>
                    	<option value="CW">Curaçao</option>
                    	<option value="CY">Cyprus</option>
                    	<option value="CZ">Czech Republic</option>
                    	<option value="DK">Denmark</option>
                    	<option value="DJ">Djibouti</option>
                    	<option value="DM">Dominica</option>
                    	<option value="DO">Dominican Republic</option>
                    	<option value="EC">Ecuador</option>
                    	<option value="EG">Egypt</option>
                    	<option value="SV">El Salvador</option>
                    	<option value="GQ">Equatorial Guinea</option>
                    	<option value="ER">Eritrea</option>
                    	<option value="EE">Estonia</option>
                    	<option value="ET">Ethiopia</option>
                    	<option value="FK">Falkland Islands (Malvinas)</option>
                    	<option value="FO">Faroe Islands</option>
                    	<option value="FJ">Fiji</option>
                    	<option value="FI">Finland</option>
                    	<option value="FR">France</option>
                    	<option value="GF">French Guiana</option>
                    	<option value="PF">French Polynesia</option>
                    	<option value="TF">French Southern Territories</option>
                    	<option value="GA">Gabon</option>
                    	<option value="GM">Gambia</option>
                    	<option value="GE">Georgia</option>
                    	<option value="DE">Germany</option>
                    	<option value="GH">Ghana</option>
                    	<option value="GI">Gibraltar</option>
                    	<option value="GR">Greece</option>
                    	<option value="GL">Greenland</option>
                    	<option value="GD">Grenada</option>
                    	<option value="GP">Guadeloupe</option>
                    	<option value="GU">Guam</option>
                    	<option value="GT">Guatemala</option>
                    	<option value="GG">Guernsey</option>
                    	<option value="GN">Guinea</option>
                    	<option value="GW">Guinea-Bissau</option>
                    	<option value="GY">Guyana</option>
                    	<option value="HT">Haiti</option>
                    	<option value="HM">Heard Island and McDonald Islands</option>
                    	<option value="VA">Holy See (Vatican City State)</option>
                    	<option value="HN">Honduras</option>
                    	<option value="HK">Hong Kong</option>
                    	<option value="HU">Hungary</option>
                    	<option value="IS">Iceland</option>
                    	<option value="IN">India</option>
                    	<option value="ID">Indonesia</option>
                    	<option value="IR">Iran, Islamic Republic of</option>
                    	<option value="IQ">Iraq</option>
                    	<option value="IE">Ireland</option>
                    	<option value="IM">Isle of Man</option>
                    	<option value="IL">Israel</option>
                    	<option value="IT">Italy</option>
                    	<option value="JM">Jamaica</option>
                    	<option value="JP">Japan</option>
                    	<option value="JE">Jersey</option>
                    	<option value="JO">Jordan</option>
                    	<option value="KZ">Kazakhstan</option>
                    	<option value="KE">Kenya</option>
                    	<option value="KI">Kiribati</option>
                    	<option value="KP">Korea, Democratic People's Republic of</option>
                    	<option value="KR">Korea, Republic of</option>
                    	<option value="KW">Kuwait</option>
                    	<option value="KG">Kyrgyzstan</option>
                    	<option value="LA">Lao People's Democratic Republic</option>
                    	<option value="LV">Latvia</option>
                    	<option value="LB">Lebanon</option>
                    	<option value="LS">Lesotho</option>
                    	<option value="LR">Liberia</option>
                    	<option value="LY">Libya</option>
                    	<option value="LI">Liechtenstein</option>
                    	<option value="LT">Lithuania</option>
                    	<option value="LU">Luxembourg</option>
                    	<option value="MO">Macao</option>
                    	<option value="MK">Macedonia, the former Yugoslav Republic of</option>
                    	<option value="MG">Madagascar</option>
                    	<option value="MW">Malawi</option>
                    	<option value="MY">Malaysia</option>
                    	<option value="MV">Maldives</option>
                    	<option value="ML">Mali</option>
                    	<option value="MT">Malta</option>
                    	<option value="MH">Marshall Islands</option>
                    	<option value="MQ">Martinique</option>
                    	<option value="MR">Mauritania</option>
                    	<option value="MU">Mauritius</option>
                    	<option value="YT">Mayotte</option>
                    	<option value="MX">Mexico</option>
                    	<option value="FM">Micronesia, Federated States of</option>
                    	<option value="MD">Moldova, Republic of</option>
                    	<option value="MC">Monaco</option>
                    	<option value="MN">Mongolia</option>
                    	<option value="ME">Montenegro</option>
                    	<option value="MS">Montserrat</option>
                    	<option value="MA">Morocco</option>
                    	<option value="MZ">Mozambique</option>
                    	<option value="MM">Myanmar</option>
                    	<option value="NA">Namibia</option>
                    	<option value="NR">Nauru</option>
                    	<option value="NP">Nepal</option>
                    	<option value="NL">Netherlands</option>
                    	<option value="NC">New Caledonia</option>
                    	<option value="NZ">New Zealand</option>
                    	<option value="NI">Nicaragua</option>
                    	<option value="NE">Niger</option>
                    	<option value="NG">Nigeria</option>
                    	<option value="NU">Niue</option>
                    	<option value="NF">Norfolk Island</option>
                    	<option value="MP">Northern Mariana Islands</option>
                    	<option value="NO">Norway</option>
                    	<option value="OM">Oman</option>
                    	<option value="PK">Pakistan</option>
                    	<option value="PW">Palau</option>
                    	<option value="PS">Palestinian Territory, Occupied</option>
                    	<option value="PA">Panama</option>
                    	<option value="PG">Papua New Guinea</option>
                    	<option value="PY">Paraguay</option>
                    	<option value="PE">Peru</option>
                    	<option value="PH">Philippines</option>
                    	<option value="PN">Pitcairn</option>
                    	<option value="PL">Poland</option>
                    	<option value="PT">Portugal</option>
                    	<option value="PR">Puerto Rico</option>
                    	<option value="QA">Qatar</option>
                    	<option value="RE">Réunion</option>
                    	<option value="RO">Romania</option>
                    	<option value="RU">Russian Federation</option>
                    	<option value="RW">Rwanda</option>
                    	<option value="BL">Saint Barthélemy</option>
                    	<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                    	<option value="KN">Saint Kitts and Nevis</option>
                    	<option value="LC">Saint Lucia</option>
                    	<option value="MF">Saint Martin (French part)</option>
                    	<option value="PM">Saint Pierre and Miquelon</option>
                    	<option value="VC">Saint Vincent and the Grenadines</option>
                    	<option value="WS">Samoa</option>
                    	<option value="SM">San Marino</option>
                    	<option value="ST">Sao Tome and Principe</option>
                    	<option value="SA">Saudi Arabia</option>
                    	<option value="SN">Senegal</option>
                    	<option value="RS">Serbia</option>
                    	<option value="SC">Seychelles</option>
                    	<option value="SL">Sierra Leone</option>
                    	<option value="SG">Singapore</option>
                    	<option value="SX">Sint Maarten (Dutch part)</option>
                    	<option value="SK">Slovakia</option>
                    	<option value="SI">Slovenia</option>
                    	<option value="SB">Solomon Islands</option>
                    	<option value="SO">Somalia</option>
                    	<option value="ZA">South Africa</option>
                    	<option value="GS">South Georgia and the South Sandwich Islands</option>
                    	<option value="SS">South Sudan</option>
                    	<option value="ES">Spain</option>
                    	<option value="LK">Sri Lanka</option>
                    	<option value="SD">Sudan</option>
                    	<option value="SR">Suriname</option>
                    	<option value="SJ">Svalbard and Jan Mayen</option>
                    	<option value="SZ">Swaziland</option>
                    	<option value="SE">Sweden</option>
                    	<option value="CH">Switzerland</option>
                    	<option value="SY">Syrian Arab Republic</option>
                    	<option value="TW">Taiwan, Province of China</option>
                    	<option value="TJ">Tajikistan</option>
                    	<option value="TZ">Tanzania, United Republic of</option>
                    	<option value="TH">Thailand</option>
                    	<option value="TL">Timor-Leste</option>
                    	<option value="TG">Togo</option>
                    	<option value="TK">Tokelau</option>
                    	<option value="TO">Tonga</option>
                    	<option value="TT">Trinidad and Tobago</option>
                    	<option value="TN">Tunisia</option>
                    	<option value="TR">Turkey</option>
                    	<option value="TM">Turkmenistan</option>
                    	<option value="TC">Turks and Caicos Islands</option>
                    	<option value="TV">Tuvalu</option>
                    	<option value="UG">Uganda</option>
                    	<option value="UA">Ukraine</option>
                    	<option value="AE">United Arab Emirates</option>
                    	<option value="GB">United Kingdom</option>
                    	<option value="US">United States</option>
                    	<option value="UM">United States Minor Outlying Islands</option>
                    	<option value="UY">Uruguay</option>
                    	<option value="UZ">Uzbekistan</option>
                    	<option value="VU">Vanuatu</option>
                    	<option value="VE">Venezuela, Bolivarian Republic of</option>
                    	<option value="VN">Viet Nam</option>
                    	<option value="VG">Virgin Islands, British</option>
                    	<option value="VI">Virgin Islands, U.S.</option>
                    	<option value="WF">Wallis and Futuna</option>
                    	<option value="EH">Western Sahara</option>
                    	<option value="YE">Yemen</option>
                    	<option value="ZM">Zambia</option>
                    	<option value="ZW">Zimbabwe</option>
                    </select>
                   </div>
                </div>

              </div>
            </div>
            <div class="row">
              <div class="col-md-5">
                <input id="nonce" name="payment_method_nonce" type="hidden" />
                <button class="btn btn-secondary" type="submit"><span>Donate</span></button>

              </div>
            </div>
            </form>
          </div>
        </section>

        <div class="container">




        <div class="row">
          <div class="offset-md-3 col-md-7">

            <h3 class="pull-left">HOW YOU CAN HELP BRIDGE THE GAP</h3>
            <p>You can help bridge the gap in by contributing to the Foundation. Donations can be made online or by cheque. All donations are tax deductible and all donors will be sent a receipt. The trustee of the Foundation will oversee the application
              of funds, but donors have the opportunity to request that their donations be prioritised for a specific aspect of the Foundation’s work. Priorities that might be considered include:</p>
            <ul>
              <li>Directing funds towards any of the specific research areas already identified by Menzies or CDU;</li>
              <li>Directing that funds be added to an endowment fund and used to fund future research initiatives at Menzies, CDU and/or the partners they work with to bridge the gap in Indigenous health and education in Australia</li>
              <li>Creating a scholarship for high-potential Indigenous researchers at Menzies or CDU who will work to bridge the gap in health and education in Indigenous communities (potentially subject to a minimum contribution to ensure significant and
                sustained support)</li>
            </ul>
            <p>
              If you wish to discuss a donation, make a donation via bank transfer or if you would like your donation to go to a specific project, please contact Colin Baillie, Head of Development: M) 0410 634 889 | Colin.Baillie@ menzies.edu.au.
            </p>
            <p>Donors are also asked to consider transforming the lives of future Indigenous communities by leaving a bequest to the Foundation in their wills. A Foundation Bequest Form will be provided on request to Colin Baillie (see details above).
            </p>
            <p>A copy of the Foundation’s DGR and ITEC certificates can be provided on request.</p>


          </div>
        </div>
      </div>



      <div class="container">
        <div class="row">
          <div class="offset-md-3 col-md-7">


            <h3 class="pull-left">Your return on investment</h3>

            <p>The most important return to you as a donor is to know that you are helping to bridge the gap in health and education disparities between Indigenous and non-Indigenous Australians. There may be no greater reward than knowing that you are part of the solution to one of Australia’s most urgent issues.</p>
            <p>Donors may also receive other intangible benefits as outlined below:</p>

            <div class="row bg-yellow">
              <div class="col-md-6">
                <h4>RELATIONSHIPS WITH RESEARCHERS</h4>
              </div>
              <div class="col-md-6">
                <p>You will receive feedback and updates from researchers and project leaders whose work is funded by the Foundation</p>
              </div>
            </div>
            <div class="row bg-grey">
              <div class="col-md-6">
                <h4>PROMOTION AND ACKNOWLEDGEMENT</h4>
              </div>
              <div class="col-md-6">
                <p>You will be acknowledged by the Foundation in publications, events and on the website. Foundation staff can assist you in promoting your involvement more broadly if required</p>
              </div>
            </div>
            <div class="row bg-yellow">
              <div class="col-md-6">
                <h4>PERSONAL PARTICIPATION</h4>
              </div>
              <div class="col-md-6">
                <p>Where appropriate, you may be invited to meet with key researchers and visit community-based projects in the field</p>
              </div>
            </div>
            <div class="row bg-grey">
              <div class="col-md-6">
                <h4>MEDIA EXPOSURE</h4>
              </div>
              <div class="col-md-6">
                <p>With your agreement, you and/or your organisation may be featured in the media as part of the Foundation’s promotion of its activities</p>
              </div>
            </div>
            <div class="row bg-yellow">
              <div class="col-md-6">
                <h4>TAX BENEFIT</h4>
              </div>
              <div class="col-md-6">
                <p>Gifts to the Foundation are deductible for taxation purposes under Subdivision 30-BA of the Income Tax Assessment ACT 1997</p>
              </div>
            </div>
            <div class="row bg-grey">
              <div class="col-md-6">
                <h4>NAMING RIGHTS</h4>
              </div>
              <div class="col-md-6">
                <p>With your agreement, long-term and perpetual scholarships can be named in honour of the kind donor who has made them possible</p>
              </div>
            </div>
          </div>
        </div>
    <div class="container">
        <div class="row">

          <div class="offset-md-3 col-md-7">
              <h2>BTGF DONOR PROMISE</h2>
              <h3>The Foundation commits to:</h3>
              <ul>
              <li>Act with integrity and be accountable </li>
              <li>Treat our stakeholders with respect, openness and honesty</li>
              <li>Respond to all queries in a professional and timely manner </li>
              <li>Use support wisely and correctly for maximum impact</li>
              <li>Ensure the privacy and confidentiality of donor data </li>
              <li>Respect the wishes of those who request changes in the way we communicate with them</li>
              <li>Work with efficiency, transparency and accuracy</li>
              <li>Champion our cause </li>
              <li>Conduct itself professionally with all third-party relationships </li>
              </ul>
              <h3>FAQS (general donation/fundraising)</h3>
              <h4>How did the Foundation start? </h4>
              <p>The Bridging the Gap Foundation was established through the Menzies School of Health Research (Menzies) and Charles Darwin University (CDU). <p/><br><br>
              <h4>What do you do? </h4>
              <p>The Bridging the Gap Foundation seeks to raise funds for projects in health and education in order to address one of Australia’s most urgent issues: the gap between Indigenous and non-Indigenous Australians’ health and education opportunities and outcomes.</p><br><br>
              <h4>Why do we need to ‘bridge the gap’? </h4>
              <p>There are many challenges currently facing our Indigenous communities, which can all be summarised in a simple statistic, there is a 10-year gap in the average life expectancy between Indigenous and non-Indigenous Australians. Health and education are major contributors to this gap. </p><br>
              <p>The Bridging the Gap Foundation aims to improve and advance the lives of Indigenous Australians by raising funds to address these challenges.</p><br><br>
              <h4>How do you spend your supporters donations? </h4>
              <p>The trustee of the Foundation will oversee the application of funds, but donors have the opportunity to request that their donations be prioritised for a specific aspect of the Foundation’s work. Priorities that might be considered include:</p><br>
              <ul>
              <li>Directing funds towards any of the specific research areas already identified by Menzies or CDU;</li>
              <li>Directing that funds be added to an endowment fund and used to fund future research initiatives at Menzies, CDU and/or the partners they work with.   </li>
              <li>Creating a scholarship for high-potential Indigenous researchers at Menzies or CDU who will work to bridge the gap in health and education in Indigenous communities (potentially subject to a minimum contribution to ensure significant and sustained support)</li>
              <li>Directing funds to the appropriate party to bridge the gap in Indigenous health and education in Australia</li>
              </ul><br><br>
              <h4>Are you DGR certified/can I get a tax deduction receipt? </h4><br>
              <p>Gifts to the Foundation are deductible for taxation purposes under Subdivision 30-BA of the Income Tax Assessment ACT 1997 and all donors will be sent a receipt. </p>



            </div>
          </div>
        </div>




    </main>


    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <a href="/"><img src="images/bridging-the-gap-logo.svg" alt="Bridging The Gap" class="main-logo img-fluid"></a>
            <div class="social">
              <a href="contact-us.html" style="text-decoration: none;"><span class="icon-icons_email"></span></a>
              <a href="https://www.facebook.com/BTGFaustralia/" style="text-decoration: none;"><span class="icon-icons_social-facebook"></span></a>
              <a href="https://twitter.com/BTGFaustralia" style="text-decoration: none;"><span class="icon-icons_social-twitter"></span></a>
            </div>

          </div>
          <div class="offset-md-1 col-md-7">
            <div class="row">
              <div class="col-md-4">
                <a class="footer-link" href="who-we-are.html"><strong>Who we are</strong></a>
                <a class="footer-link" href="charles-darwin-university.html">CDU</a>
                <a class='footer-link' href="menzies-school.html">Menzies</a>
                <a class="footer-link" href="our-board.html">Board</a>
                <a class="footer-link" href="our-researchers.html">Researchers</a>
               
                <a class="footer-link" href="staff.html">Staff</a>
              </div>
              <div class="col-md-4">
                <a class="footer-link" href="how.html"><strong>Key projects</strong></a>
                <a class="footer-link" href="who.html">What we do</a>
                <a class="footer-link" href="about-us.html">Our mission</a>
                <a class="footer-link" href="latest-news.html">Latest News</a>

              </div>
              <div class="col-md-4">

                <!--<a class="footer-link" href="terms-conditions.html">Terms and Conditions</a>-->
                <a class="footer-link" href="sponsors.php"><strong>Sponsors &<br/>Donors</strong></a>
                <a class="footer-link" href="privacy.html">Privacy</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>



    </div>



    <script src="https://js.braintreegateway.com/web/dropin/1.9.3/js/dropin.min.js"></script>
    <script>
        var form = document.querySelector('#payment-form');
        var client_token = "<?php echo(Braintree\ClientToken::generate()); ?>";
        var deviceDataInput = form['device_data'];

        braintree.dropin.create({
          authorization: client_token,
          selector: '#bt-dropin',
          paypal: {
            flow: 'vault'
          },
          dataCollector: {
            kount: true, // Required if Kount fraud data collection is enabled
            paypal: true // Required if PayPal fraud data collection is enabled
          }
        }, function (createErr, instance) {
          if (deviceDataInput == null) {
            deviceDataInput = document.createElement('input');
            deviceDataInput.name = 'device_data';
            deviceDataInput.type = 'hidden';
            form.appendChild(deviceDataInput);
          }
          if (createErr) {
            return;
          }
          form.addEventListener('submit', function (event) {
            event.preventDefault();

            instance.requestPaymentMethod(function (err, payload) {
              if (err) {
                return;
              }

              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.getElementsByClassName('needs-validation');
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                //form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                    event.stopPropagation();
                  } else {
	                  document.querySelector('#nonce').value = payload.nonce;
                    deviceDataInput.value = payload.deviceData;
					          form.submit();
                  }
                  form.classList.add('was-validated');
              });

            });
          });
        });
    </script>



    <!-- Modal -->
    <div class="modal fade" id="vidModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body" id="modalBody">
          </div>
        </div>
      </div>
    </div>

    <script>
      (function(b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
          function() {
            (b[l].q = b[l].q || []).push(arguments)
          });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
      }(window, document, 'script', 'ga'));
      ga('create', 'UA-114141145-1');
      ga('send', 'pageview');
    </script>

    <script src="scripts/vendor.js"></script>
    <script src="scripts/main.js"></script>

</body>

</html>
