<?php
session_start();
require_once("vendor/autoload.php");

if(file_exists(__DIR__ . "/.env")) {
    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();
}

Braintree\Configuration::environment(getenv('BT_ENVIRONMENT'));
Braintree\Configuration::merchantId(getenv('BT_MERCHANT_ID'));
Braintree\Configuration::publicKey(getenv('BT_PUBLIC_KEY'));
Braintree\Configuration::privateKey(getenv('BT_PRIVATE_KEY')); ?>

<!doctype html>
<html lang="">
<head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bridging the Gap Foundation - Donate</title>

  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- build:css styles/vendor.css -->
  <!-- bower:css -->
  <!-- endbower -->
  <!-- endbuild -->

  <!-- build:css styles/main.css -->
  <link rel="stylesheet" href="styles/main.css">
  <!-- endbuild -->

  <script src="https://js.braintreegateway.com/web/dropin/1.9.3/js/dropin.min.js"></script>

</head>

<body>

  <header class="main">
      <div class="notice-wrapper">
          <?php if(isset($_SESSION["errors"])) : ?>
              <div class="show notice error notice-error">
                  <span class="notice-message">
                      <?php
                          echo($_SESSION["errors"]);
                          unset($_SESSION["errors"]);
                      ?>
                  <span>
              </div>
          <?php endif; ?>
      </div>
  </header>



  <div id="app">
    <div class="nav-top-bar">
      <div class="play-arrow"><a href="" id="play-vid"><span class="play-vid-inner"><span class="d-none d-md-block">Play Video</span></span></a></div>
      <div class="donate-btn-top">
        <a href="donate.php" class="btn">Donate</a>
      </div>

    </div>
<div class="donate-bar nav-up">
      <div class="container">
        <div class="main-nav">
          <a href="who-we-are.html" class="main-nav-item">Who we are</a>
          <a href="who.html" class="main-nav-item">What we do</a>
          <a href="about-us.html" class="main-nav-item">Our Mission</a>
          <a href="how.html" class="main-nav-item">Key Projects</a>
          <a href="latest-news.html" class="main-nav-item">Latest News</a>
          <a href="contact-us.html" class="main-nav-item">Contact</a>
        </div>
      </div>
      <button class="d-xl-none hamburger hamburger--squeeze" type="button" id="hamburger">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
    </div>
    <section class="fullwidth fixed sub overlap-bot" style="background-image: url(images/fullwidth-toddler3.jpg);">
      <div class="banner-wrap">
        <nav class="topnav">

          <div class="logo-group">
            <a href="/" class="top-row-link"><img src="images/bridging-the-gap-logo.svg" alt="Bridging The Gap" class="main-logo img-fluid"></a>
            <div class="bott-row">
              <div class="left-col">
                <img src="images/menzies-logo-white.svg" alt="Menzies Logo" class="menzies-logo img-fluid">
              </div>
              <div class="right-col">
                <img src="images/charles-darwin-logo-white.svg" alt="Charles Darwin University Logo" class="charles-logo img-fluid">
              </div>
            </div>
          </div>

        </nav>
      </div>
    </section>
    <main class="content">
      <div class="container">
        <div class="row">
          <div class="offset-md-2 offset-lg-3 col-md-7 col-lg-6">
            <div class="intro-sub">
            </div>
            <h2 class="m-top-tight fluid">Donate</h2>
          </div>
        </div>
        <!-- <div class="row">
          <div class="offset-md-3 col-md-7">
            <h3 class="pull-left">HOW YOU CAN HELP BRIDGE THE GAP</h3>
          </div>
        </div> -->
      </div>



              <?php


                  if (isset($_GET["id"])) {
                      $transaction = Braintree\Transaction::find($_GET["id"]);

                      $transactionSuccessStatuses = [
                          Braintree\Transaction::AUTHORIZED,
                          Braintree\Transaction::AUTHORIZING,
                          Braintree\Transaction::SETTLED,
                          Braintree\Transaction::SETTLING,
                          Braintree\Transaction::SETTLEMENT_CONFIRMED,
                          Braintree\Transaction::SETTLEMENT_PENDING,
                          Braintree\Transaction::SUBMITTED_FOR_SETTLEMENT
                      ];

                      if (in_array($transaction->status, $transactionSuccessStatuses)) {
                          $header = "Success";
                          $icon = "success";
                          $message = "Thank you for your donation. You will receive a receipt at the email address provided.";
                      } else {
                          $header = "Transaction Failed - " . $transaction->status;
                          $icon = "fail";
                          $message = "Something went wrong. Please try again or contact us if the issue persists.";
                      }
                  }
              ?>

             <section class="donate">
		        <div class="container">
		          <div class="row">
		            <div class="col-md-5 offset-md-3">


                          <h3><?php echo($header)?></h3>
                          <section>
	                          <div class="icon">
	                          	<img src="images/<?php echo($icon)?>.svg" alt="">
	                          </div>
                              <p><?php echo($message)?></p>
                          </section>
                          <section>
                              <a class="btn btn-default back" href="donate.php">
                                  <span>Back</span>
                              </a>
                          </section>
                      </div>
                  </div>
              </div>

              </section>




    </main>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <a href="/"><img src="images/bridging-the-gap-logo.svg" alt="Bridging The Gap" class="main-logo img-fluid"></a>
            <div class="social">
              <a href="contact-us.html" style="text-decoration: none;"><span class="icon-icons_email"></span></a>
              <a href="https://www.facebook.com/BTGFaustralia/" style="text-decoration: none;"><span class="icon-icons_social-facebook"></span></a>
              <a href="https://twitter.com/BTGFaustralia" style="text-decoration: none;"><span class="icon-icons_social-twitter"></span></a>
            </div>
          </div>
          <div class="offset-md-1 col-md-7">
            <div class="row">
              <div class="col-md-4">
                <a class="footer-link" href="who-we-are.html"><strong>Who we are</strong></a>
                <a class="footer-link" href="charles-darwin-university.html">CDU</a>
                <a class='footer-link' href="menzies-school.html">Menzies</a>
                <a class="footer-link" href="our-board.html">Board</a>
                <a class="footer-link" href="our-researchers.html">Researchers</a>
             
                <a class="footer-link" href="staff.html">Staff</a>
              </div>
              <div class="col-md-4">
                <a class="footer-link" href="how.html"><strong>Key projects</strong></a>
                <a class="footer-link" href="who.html">What we do</a>
                <a class="footer-link" href="about-us.html">Our mission</a>
                <a class="footer-link" href="#">Latest News</a>
              </div>
              <div class="col-md-4">
                <!--<a class="footer-link" href="terms-conditions.html">Terms and Conditions</a>-->
                <a class="footer-link" href="sponsors.php"><strong>Sponsors &<br/>Donors</strong></a>
                <a class="footer-link" href="privacy.html">Privacy</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    </div>






</body>
</html>
